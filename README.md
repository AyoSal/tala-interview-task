# Backup of Archive Tables in RDS with Ansible#

### Requirements ###

* Python 2.7
* boto
* boto3
* Ansible >= 2.4
* AWS_ACCESS_KEY and AWS_SECRET_KEY environment variables

### Usage ###
* Update group_vars/db.yml with appropriate properties to suite your environment:
    ```yaml
    database: <Database to backup>
    aws_backup_bucket: <s3 bucket to store backup files>
    aws_region: <AWS region>
    scratch_dir: <Directory to store temporary backup files on database server>
    mysql_user: <MySQL backup user>
    mysql_password: <MySQL backup user password>
    ```
* Update hosts file with appropriate ansible_ssh_user (currently set to ayosal)
* Run 
    ```
    ansible-playbook -i hosts Master_playbook.yml
    ```